var nombre = 'Ulises Axel Lopez German';
var num_empleado = '90112002';
var ip_equipo = '10.44.31.207 <br/> 10.59.29.121';

var url = new URL(window.location.href);

var ip = null;
var idx = null;

var oferta = url.searchParams.get("RNP_DB") ? 'RNP_DB' : (url.searchParams.get("P_DB") ? 'P_DB' : null);

if(oferta){
    ip = url.searchParams.get(oferta);
    if(ip){

        var ips = [
            '10.50.2.97', /* regionalizacion */
            '10.44.2.69', /* tienda.0800 */
            '10.44.16.170', /* correosecommercev2 */
            '10.44.1.159', /* tiendavirtual */
            '10.62.10.84', /* CoppelDB */
            '10.44.1.167' /* solicitud_credito */
        ];

        idx = ips.indexOf(ip); 

        var seleccionar = [
            '10.50.2.97 - regionalizacion',
            '10.44.2.69 - tienda.0800',
            '10.44.16.170 - correosecommercev2',
            '10.44.1.159 - tiendavirtual',
            '10.62.10.84 - COPPELDB',
            '10.44.1.167 - solicitud_credito'
        ];

        var asunto = [
            'Renovacion de permisos de DB - regionalizacion',
            'Renovacion de permisos de DB - tienda.0800',
            'Renovacion de permisos de DB - correosecommercev2',
            'Renovacion de permisos de DB - tiendavirtual',
            'Renovacion de permisos de DB - COPPELDB',
            'Renovacion de permisos de DB - solicitud_credito',
        ]; 

        var msj_desc = 'Se solicita de su apoyo para la renovacion de permisos a la base de datos ya que que las credenciales han vencido.'
        var descripcion = [
            `${msj_desc}<br/><br/> DB: ${seleccionar[idx]}<br/><br/> Nombre: ${nombre}<br/> Numero de empleado: ${num_empleado}<br/> IP equipo: <br/>${ip_equipo}`,
            `${msj_desc}<br/><br/> DB: ${seleccionar[idx]}<br/><br/> Nombre: ${nombre}<br/> Numero de empleado: ${num_empleado}<br/> IP equipo: <br/>${ip_equipo}`,
            `${msj_desc}<br/><br/> DB: ${seleccionar[idx]}<br/><br/> Nombre: ${nombre}<br/> Numero de empleado: ${num_empleado}<br/> IP equipo: <br/>${ip_equipo}`,
            `${msj_desc}<br/><br/> DB: ${seleccionar[idx]}<br/><br/> Nombre: ${nombre}<br/> Numero de empleado: ${num_empleado}<br/> IP equipo: <br/>${ip_equipo}`,
            `${msj_desc}<br/><br/> DB: ${seleccionar[idx]}<br/><br/> Nombre: ${nombre}<br/> Numero de empleado: ${num_empleado}<br/> IP equipo: <br/>${ip_equipo}`,
            `${msj_desc}<br/><br/> DB: ${seleccionar[idx]}<br/><br/> Nombre: ${nombre}<br/> Numero de empleado: ${num_empleado}<br/> IP equipo: <br/>${ip_equipo}`
        ];

        var Interval = function() {
            var b = setInterval(function() {
                console.log('intervalo..');
            Array.from(document.getElementsByClassName('selectListUserItemName')).forEach(function (element) {
                console.log('buscando ip');
                if(element.innerText.match(seleccionar[idx])){ 
                    element.click();
                    document.querySelector('div[id="user_select_save_button"] > a').click();
                    clearInterval(b);
                    delete(b);
                    setTimeout(() => {
                        document.querySelector('div[data-name="field_uid_14985"]>div[class="inputMultipleSelect"]').click();
                        setTimeout(() => {
                            document.querySelector('div[data-value="A683BF82"]').click();
                            setTimeout(() => {
                                var date = new Date();
                                document.querySelector('input[name="field_uid_14987[date]"]').value = `${date.getFullYear()}-${(date.getMonth() +1)}-${date.getDate()}`;
                                document.querySelector('input[name="field_uid_14987[time]"]').value = `${date.getHours()}:${date.getMinutes()}`;
                                setTimeout(() => {                        
                                    document.querySelector('input[id="form_create_subject"]').value = asunto[idx];
                                    CKEDITOR.instances['form_create_description'].setData(descripcion[idx]);
                                }, 1000);
                            }, 1000);
                        }, 2000);
                    }, 2000);
                }  
            });
            }, 1000);
        }

        setTimeout(() => {
            document.querySelector('div[data-value="1466"]').click(); 
            setTimeout(() => {
                document.querySelector('div[data-value="1491"]').click();
                setTimeout(() => {
                    document.querySelector('div[data-value="3379"]').click();
                    setTimeout(() => {
                        document.querySelector('div[data-value="3382"]').click();
                        setTimeout(() => {
                            document.querySelector('div[class="new-request-catalog__column-header-button button-blue createRequestButton"]').click();
                            setTimeout(() => {

                                document.querySelector('div[data-name="field_uid_1"]>div[class="inputMultipleSelect"]').click();
                                setTimeout(() => {
                                    document.querySelector('div[data-value="4"][title="Usuario / Cliente"]').click();
                        
                                    setTimeout(() => {
                                        document.querySelector('div[data-name="field_uid_2"]>div[class="inputMultipleSelect"]').click();
                                        setTimeout(() => {
                                            document.querySelector('div[data-value="4"]').click();
                                            setTimeout(() => {
                                                document.querySelector('div[id="custom_field_entry_4230"]').click();
                                                setTimeout(() => {
                                                    var a = document.querySelector('div[class="inputSearchContainer"] > input[value="Todos..."]');
                                                    a.value = ips[idx];
                                                    a.dispatchEvent(new Event('keyup'));
                                                    c = new Interval(); 
                                                }, 2000);
                                            }, 2000);
                                        }, 1000);
                                    }, 1000);
                                }, 1000);
                            }, 3000);
                        }, 1000);
                    }, 1000);
                }, 1000);
            }, 1000);    
        }, 1000);
    }
}